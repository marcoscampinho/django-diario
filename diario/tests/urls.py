from django.conf.urls.defaults import patterns, include, url


urlpatterns = patterns('',
    url(r'^', include('diario.urls.entries')),
    url(r'^', include('diario.urls.entries_by_author')),
    url(r'^', include('diario.urls.tagged'))
)

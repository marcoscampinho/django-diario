from django.conf import settings
from django.test import TestCase
from django.test.client import Client


class TestList(TestCase):
    fixtures = ['auth_testdata.json', 'entries_testdata.json']
    urls = 'diario.tests.urls'
    
    def setUp(self):
        self.c = Client()

    def testAnonymousList(self):
        result = self.c.get('/')
        self.assertEqual(result.status_code, 200)
            

    def testAuthenticatedList(self):
        is_ok = self.c.login(username='developer', password='developer')
        self.assertTrue(is_ok, "Couldn't logged in")
        
        result = self.c.get('/')
        self.assertEqual(result.status_code, 200)
        

    def testListDay(self):
        result = self.c.get('/2011/11/11/')
        self.assertEqual(result.status_code, 200)
        

    def testListMonth(self):
        result = self.c.get('/2011/11/')
        self.assertEqual(result.status_code, 200)


    def testListYear(self):
        result = self.c.get('/2011/')
        self.assertEqual(result.status_code, 200)


class TestDetail(TestCase):
    fixtures = [ 'auth_testdata.json', 'entries_testdata.json' ]
    urls = 'diario.tests.urls'

    def setUp(self):
        self.c = Client()


    def testDetailDraft(self):        
        result = self.c.get('/2011/11/12/test-draft/')         
        self.assertEqual(result.status_code, 404)
        

    def testDetailEntry(self):
        result = self.c.get('/2011/11/11/test-entry/')
        self.assertEqual(result.status_code, 200)

      

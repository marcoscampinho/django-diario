# -*- coding: utf-8 -*-

from tagging.views import tagged_object_list
from diario.models import Entry

def tagged_entry_list(request, *args, **kwargs):
    """
    A thin wrapper around ``tagging.views.tagged_object_list``.
    """
    if 'queryset_or_model' not in kwargs:
        kwargs['queryset_or_model'] = Entry.published_on_site.all()
    return tagged_object_list(request, *args, **kwargs)

# -*- coding: utf-8 -*-

from diario.models import Entry
from django.views.generic.list import ListView
from django.views.generic import dates
from diario.settings import DIARIO_NUM_LATEST


class EntryDetail(dates.DateDetailView):
    slug_field = 'slug'
    date_field = 'pub_date'
    queryset = Entry.published_on_site.all()
    month_format = '%m'

    def dispatch(self, request, *args, **kwargs):
        if request.user.has_perm('diario.change_entry'):
            self.allow_future = True
            self.queryset = Entry.on_site.all()
        else:
            self.allow_future = False
        return super(EntryDetail, self).dispatch(request, *args, **kwargs)


class EntryArchiveYear(dates.YearArchiveView):
    queryset = Entry.published_on_site.all()
    date_field = 'pub_date'


class EntryArchiveMonth(dates.MonthArchiveView):
    month_format = '%m'
    date_field = 'pub_date'
    queryset = Entry.published_on_site.all()


class EntryArchiveDay(dates.DayArchiveView):
    month_format = '%m'
    date_field = 'pub_date'
    queryset = Entry.published_on_site.all()


class EntryList(ListView):
    queryset = Entry.published_on_site.all()
    paginate_by = DIARIO_NUM_LATEST

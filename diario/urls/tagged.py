# -*- coding: utf-8 -*-

"""URL definitions for weblog entries divided by tag"""

from django.conf.urls.defaults import *

from diario.settings import DIARIO_NUM_LATEST
from diario.feeds import tagged as feeds


# TODO: TagListView
# author_list = url(
#     regex  = '^$',
#     view   = views.TagListView.as_view(),    
#     name   = 'diario-tagged-tag-list',
# )

tagged_entry_list = url(        # entries by tag
    regex  = '^(?P<tag>[^/]+)/$',
    view   = 'diario.views.tagged.tagged_entry_list',
    name   = 'diario-tagged-entry-list',
    kwargs = {
        'paginate_by': DIARIO_NUM_LATEST,
        'template_name': 'diario/entry_list_tagged.html',
        'template_object_name': 'entry',
    }
)


tagged_entry_atom = url(
    regex = '^(?P<tag>[^/]+)/atom/$',
    view  = feeds.AtomEntriesByTagFeed(),
    name = 'diario-tagged-entry-atom'
)

tagged_entry_rss = url(
    regex = '^(?P<tag>[^/]+)/rss/$',
    view  = feeds.RssEntriesByTagFeed(),
    name = 'diario-tagged-entry-rss'
)


urlpatterns = patterns('',
    tagged_entry_list,
    tagged_entry_atom, tagged_entry_rss
)

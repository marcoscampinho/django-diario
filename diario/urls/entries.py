# -*- coding: utf-8 -*-

"""URL definitions for weblog entries"""

from django.conf.urls import url, include

from diario.feeds import entries as feeds
from diario.feeds import comments as comment_feeds
from diario.views import entries as views


entry_list = url(
    regex  = '^$',
    view   = views.EntryList.as_view(),
    name   = 'diario-entry-list'
)

entry_detail = url(
    regex  = '^(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$',
    view   = views.EntryDetail.as_view(),
    name   = 'diario-entry'
)


archive_day = url(
    regex  = '^(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\d{2})/$',
    view   = views.EntryArchiveDay.as_view(),
    name   = 'diario-archive-day'
)

archive_month = url(
    regex  = '^(?P<year>\d{4})/(?P<month>[0-9]{2})/$',
    view   = views.EntryArchiveMonth.as_view(),
    name   = 'diario-archive-month'
)

archive_year = url(
    regex  = '^(?P<year>\d{4})/$',
    view   = views.EntryArchiveYear.as_view(),
    name   = 'diario-archive-year'
)


entries_atom = url(
    regex = '^atom/$',
    view  = feeds.AtomEntriesFeed(),
    name  = 'diario-entry-atom'
)

entries_rss = url(
    regex = '^rss/$',
    view  = feeds.RssEntriesFeed(),
    name  ='diario-entry-rss'
)


entry_comments_atom = url(
    regex = '^(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/atom/$',
    view  = comment_feeds.AtomCommentsByEntryFeed(),
    name  = 'diario-entry-comments-atom'
)

entry_comments_rss = url(
    regex = '^(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/rss/$',
    view  = comment_feeds.RssCommentsByEntryFeed(),
    name  ='diario-entry-comments-rss'
)


urlpatterns = [
    entry_list, entry_detail,
    archive_day, archive_month, archive_year,
    entries_atom, entries_rss,
    entry_comments_atom, entry_comments_rss
]

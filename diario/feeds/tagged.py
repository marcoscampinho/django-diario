# -*- coding: utf-8 -*-

from django.contrib.syndication.views import Feed
from django.contrib.syndication.views import FeedDoesNotExist
from django.utils.feedgenerator import Atom1Feed
from django.contrib.sites.models import Site
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext as _

from diario.feeds.entries import RssEntriesFeed
from diario.models import Entry
from tagging.models import Tag


class RssEntriesByTagFeed(RssEntriesFeed):

    def get_object(self, request, tag):
        return get_object_or_404(Tag, name__exact=tag)

    def title(self, tag):
        if not hasattr(self, '_site'):
            self._site = Site.objects.get_current()
        return _('%(title)s\'s Weblog: entries tagged "%(tag name)s"') % \
               {'title': self._site.name, 'tag name': tag.name}

    def description(self, tag):
        return _('Latest entries for tag "%(tag name)s"') % \
               {'tag name': tag.name}

    def link(self, tag):
        if not tag:
            raise FeedDoesNotExist
        return reverse('diario-tagged-entry-list', args=[tag.name])

    def get_query_set(self, tag):
        queryset = Entry.published_on_site.filter(tags__contains=tag.name)
        return queryset.order_by('-pub_date')

    def items(self, tag):
        return self.get_query_set(tag)[:15]

class AtomEntriesByTagFeed(RssEntriesByTagFeed):
    feed_type = Atom1Feed
    subtitle = RssEntriesByTagFeed.description

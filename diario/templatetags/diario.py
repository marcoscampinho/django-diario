# -*- coding: utf-8 -*-

"""Diário Template Tags

The ``diario.templatetags.diario`` module defines a number of template
tags which may be used to work with entries and its tags, categories
and so on.

To access Diário entries template tags in a template, use the {% load %}
tag::

    {% load diario %}

"""

from django import template

from .. import models


register = template.Library()


def get_latest_entries(n):
    """Gets the latest N entries and populates the template context
    with a variable containing a list Entry objects.

    Syntax::

       {% get_latest_entries [num] as [var_name] %}

    Example usage to get latest entries::

       {% get_latest_entries 10 as latest_entries %}

    """
    return models.Entry.objects.visible()[:n]


def get_featured_entries(n):
    return models.Entry.objects.featured()[:n]


def get_categories():
    # FIXME: must return only categories with visible entries
    return models.Category.objects.all()


def get_archive():
    qs = models.Entry.objects.visible()
    return qs.dates('published_at', 'month', 'DESC')


register.simple_tag(get_latest_entries)
register.simple_tag(get_categories)
register.simple_tag(get_archive)
register.simple_tag(get_featured_entries)

# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.contrib.syndication.views import Feed, FeedDoesNotExist
from django.core.urlresolvers import reverse, NoReverseMatch
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext as _

from diario import app_settings
from diario.models import Entry


class EntriesFeed(Feed):
    title_template = 'diario/feeditem_title.html'
    description_template = 'diario/feeditem_body.html'

    def title(self):
        """Site name"""
        if not hasattr(self, '_site'):
            self._site = Site.objects.get_current()
        return self._site.name

    def link(self):
        """Site address"""
        return reverse('diario:entry-list')

    def description(self):
        """Feed description"""
        return _('Latest entries')

    def get_query_set(self):
        return Entry.objects.visible()

    def items(self):
        return self.get_query_set()[:15]

    def item_author_name(self, entry):
        return entry.user

    def item_author_link(self, entry):
        username = entry.user.username
        try:
            return reverse('diario:user', args=[username])
        except NoReverseMatch:
            pass

    def item_pubdate(self, entry):
        return entry.published_at

    def item_categories(self, entry):
        return entry.categories.values_list('name', flat=True)


class UserFeed(EntriesFeed):
    def get_object(self, request, user):
        return get_object_or_404(User, username__exact=user)

    def title(self, user):
        """User's blog name"""
        if hasattr(user, 'get_blog_name'):
            title = user.get_blog_name()
        else:
            if not hasattr(self, '_site'):
                self._site = Site.objects.get_current()
            title = _('{} at {}').format(user, self._site.name)
        return title

    def link(self, user):
        """User's blog address"""
        if not user:
            raise FeedDoesNotExist
        return reverse('diario:user', args=[user.username])

    def description(self, user):
        """User's feed description"""
        if hasattr(user, 'get_blog_description'):
            description = user.get_blog_description()
        else:
            description = _('Latest entries by {}'.format(user))
        return description

    def get_query_set(self, user):
        return Entry.objects.visible().filter(user=user)

    def items(self, user):
        return self.get_query_set(user)[:15]

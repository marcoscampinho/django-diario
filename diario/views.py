# -*- coding: utf-8 -*-

from django.contrib import auth
from django.shortcuts import get_object_or_404
from django.views.decorators.cache import never_cache
from django.views.generic import dates, DetailView, ListView

from taggit.models import Tag

from . import app_settings, feeds, models


class BaseEntryView(object):
    date_field = 'time_published'
    month_format = '%m'
    paginate_by = app_settings.PAGINATE_BY
    queryset = models.Entry.objects.visible()
    slug_field = 'slug'


class EntryDetailView(BaseEntryView, DetailView):
    def get_template_names(self):
        return [
            'diario/entry_{}.html'.format(self.get_object().id),
        ] + super(EntryDetailView, self).get_template_names()


class EntryPreviewView(EntryDetailView):
    """Allow a authorized user preview a entry even if it is not visible.
    """
    queryset = models.Entry.objects.all()

    @never_cache
    def dispatch(self, *args, **kwargs):
        return super(EntryPreviewView, self).dispatch(*args, **kwargs)

    def get_object(self, *args, **kwargs):
        obj = super(EntryPreviewView, self).get_object(*args, **kwargs)
        user = self.request.user
        if user.is_superuser or user == obj.user:
            return obj
        elif user.has_perm('diario.can_change_other_user_entry'):
            return obj
        raise models.Entry.DoesNotExist


class EntryListView(BaseEntryView, ListView):
    pass


class ArchiveIndexView(BaseEntryView, dates.ArchiveIndexView):
    template_name = 'diario/archive.html'


class YearArchiveView(BaseEntryView, dates.YearArchiveView):
    template_name = 'diario/archive_year.html'


class MonthArchiveView(BaseEntryView, dates.MonthArchiveView):
    template_name = 'diario/archive_month.html'


class DayArchiveView(BaseEntryView, dates.DayArchiveView):
    template_name = 'diario/archive_day.html'


class CategoryView(EntryListView):
    def get_template_names(self):
        return [
            'diario/category_{}.html'.format(self.category.id),
            'diario/category.html',
        ] + super(CategoryView, self).get_template_names()

    def get_queryset(self):
        slug = self.kwargs['slug']
        self.category = get_object_or_404(models.Category, slug=slug)
        queryset = super(CategoryView, self).get_queryset()
        return queryset.filter(categories=self.category)

    def get_context_data(self, **kwargs):
        context = super(CategoryView, self).get_context_data(**kwargs)
        context['category'] = self.category
        return context


class TagView(EntryListView):
    def get_template_names(self):
        return [
            'diario/tag_{}.html'.format(self.tag.id),
            'diario/tag.html',
        ] + super(TagView, self).get_template_names()

    def get_queryset(self):
        self.tag = get_object_or_404(Tag, slug=self.kwargs['slug'])
        queryset = super(TagView, self).get_queryset()
        return queryset.filter(tags=self.tag)

    def get_context_data(self, **kwargs):
        context = super(TagView, self).get_context_data(**kwargs)
        context['tag'] = self.tag
        return context


class UserView(EntryListView):
    def get_template_names(self):
        return [
            'diario/user_{}.html'.format(self.user.id),
            'diario/user.html',
        ] + super(UserView, self).get_template_names()

    def get_queryset(self):
        username = self.kwargs['username']
        self.user = get_object_or_404(auth.get_user_model(), username=username)
        queryset = super(UserView, self).get_queryset()
        return queryset.all().visible().filter(user=self.user)

    def get_context_data(self, **kwargs):
        context = super(UserView, self).get_context_data(**kwargs)
        context['user'] = self.user
        return context


entry_detail = EntryDetailView.as_view()
entry_preview = EntryPreviewView.as_view()
entry_list = EntryListView.as_view()

category = CategoryView.as_view()
tag = TagView.as_view()
feed = feeds.EntriesFeed()

archive_index = ArchiveIndexView.as_view()
year_archive = YearArchiveView.as_view()
month_archive = MonthArchiveView.as_view()
day_archive = DayArchiveView.as_view()

user = UserView.as_view()
user_feed = feeds.UserFeed()
